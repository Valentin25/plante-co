package plant.valentin.com.flower

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.view.View
import android.widget.Button
import android.widget.EditText

import kotlinx.android.synthetic.main.settings_layout.*


class Settings : AppCompatActivity() {

    lateinit var msUsers:EditText
    lateinit var msPassword:EditText
    lateinit var msUrl:EditText
    lateinit var msPort:EditText
    lateinit var msSaveSettings: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_layout)
        setSupportActionBar(toolbar)

        msUsers = findViewById(R.id.editUser)
        msPassword = findViewById(R.id.editPass)
        msUrl = findViewById(R.id.editUrl)
        msPort = findViewById(R.id.editPort)
        msSaveSettings = findViewById(R.id.SaveSetting)

        val extras = intent.extras
        if (extras == null){
            msUsers.setText("")
            msPassword.setText("")
            msUrl.setText("")
            msPort.setText("")
        }
        else {
            msUsers.setText(extras.getString("User"))
            msPassword.setText(extras.getString("Password"))
            msUrl.setText(extras.getString("Server"))
            msPort.setText(extras.getString("Port"))

        }

        msSaveSettings.setOnClickListener (object : View.OnClickListener{
            override fun onClick(v: View?) {
               var i: Intent = Intent(this@Settings,MainActivity::class.java)
                i.putExtra("User",msUsers.text.toString())
                i.putExtra("Password",msPassword.text.toString())
                i.putExtra("Url",msUrl.text.toString() +":" + msPort.text.toString())
                startActivity(i)
                finish()


            }
            })

    }



}
