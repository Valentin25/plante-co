package plant.valentin.com.flower.clientMqtt

import android.content.Context
import android.util.Log

import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions
import org.eclipse.paho.client.mqttv3.IMqttActionListener
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.IMqttToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import java.io.UnsupportedEncodingException

class ClientMqtt(context: Context,val serverUri:String,val clientId:String,val subscriptionTopic:String,val username:String,val password:String) {

    var connected:Boolean = false
    var mqttAndroidClient: MqttAndroidClient


    init {
        mqttAndroidClient = MqttAndroidClient(context, serverUri, clientId)

        mqttAndroidClient.setCallback(object : MqttCallbackExtended {
            override
            fun connectComplete(b: Boolean, s: String) {
                Log.w("mqtt", s)
                connected = false

            }
            override
            fun connectionLost(throwable: Throwable) {

            }

            @Throws(Exception::class)
            override
            fun messageArrived(topic: String, mqttMessage: MqttMessage) {
                Log.w("Mqtt", mqttMessage.toString())
            }
            override
            fun deliveryComplete(iMqttDeliveryToken: IMqttDeliveryToken) {

            }
        })
        connect()
    }

    fun setCallback(callback: MqttCallbackExtended) {
        mqttAndroidClient.setCallback(callback)
    }

    private fun connect() {
        val mqttConnectOptions = MqttConnectOptions()
        mqttConnectOptions.setAutomaticReconnect(true)
        mqttConnectOptions.setCleanSession(false)
        mqttConnectOptions.setUserName(username)
        mqttConnectOptions.setPassword(password.toCharArray())

        try {

            mqttAndroidClient.connect(mqttConnectOptions, null, object : IMqttActionListener {
                override
                fun onSuccess(asyncActionToken: IMqttToken) {

                    val disconnectedBufferOptions = DisconnectedBufferOptions()
                    disconnectedBufferOptions.setBufferEnabled(true)
                    disconnectedBufferOptions.setBufferSize(100)
                    disconnectedBufferOptions.setPersistBuffer(false)
                    disconnectedBufferOptions.setDeleteOldestMessages(false)
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions)
                    subscribeToTopic()



                }



                override
                fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Log.w("Mqtt b", "Failed to connect to: $serverUri$exception")
                    connected = false
                }
            })


        } catch (ex: MqttException) {
            ex.printStackTrace()
        }

    }


    private fun subscribeToTopic() {
        try {
            mqttAndroidClient.subscribe(subscriptionTopic, 0, null, object : IMqttActionListener {
                override
                fun onSuccess(asyncActionToken: IMqttToken) {
                    Log.w("Mqtt", "Subscribed!")
                    connected = true
                }
                override
                fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    Log.w("Mqtt", "Subscribed fail!")
                }
            })

        } catch (ex: MqttException) {
            System.err.println("Exception whilst subscribing")
            ex.printStackTrace()
        }

    }

    fun sendData(){
        var topic = "motor/"
        var payload = "ok"
        var encodedPayload: ByteArray = ByteArray(0)

        try {
            encodedPayload = payload.toByteArray()
            var message: MqttMessage = MqttMessage(encodedPayload)
            mqttAndroidClient.publish(topic, message)
        } catch (e: UnsupportedEncodingException) {

        }
    }
}


