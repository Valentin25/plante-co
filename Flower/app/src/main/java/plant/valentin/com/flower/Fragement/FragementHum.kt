package plant.valentin.com.flower.Fragement

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import plant.valentin.com.flower.R



class FragementHum: Fragment() {


    companion object {

        fun newInstance(): FragementHum {
            return FragementHum()
        }
    }

    //3
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_humidity, container, false)
    }
}