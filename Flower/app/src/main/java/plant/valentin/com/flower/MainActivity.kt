package plant.valentin.com.flower


import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.design.widget.VisibilityAwareImageButton
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextClock
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import com.ekn.gruzer.gaugelibrary.ArcGauge
import com.github.mikephil.charting.animation.ChartAnimator
import com.github.mikephil.charting.charts.LineChart
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.w3c.dom.Text
import plant.valentin.com.flower.Fragement.*
import plant.valentin.com.flower.clientMqtt.ClientMqtt
import plant.valentin.com.flower.clientMqtt.ClientMqttSend
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.util.*
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,FragementDashBoard.OnButtonClickedListener {

    lateinit var mqttHelper: ClientMqtt
    lateinit var  mqttSender : ClientMqttSend
    var connect: Boolean = false
    var msUser:User = User("zzwusncv","qggmskLbgw3G","13447","farmer.cloudmqtt.com","sensor/temp","PlantCo")
    //var msConnect:TextView = findViewById(R.id.connectedd)
    //var mstemp:ArcGauge = findViewById(R.id.arcGauge)
    private var fragmentDashBoard: Fragment? = null
    private var fragmentTemp: Fragment? = null
    private var fragmentHumidity: Fragment? = null
    private var fragmentTool: Fragment? = null
    private var fragmentConnect: Fragment? = null

    var msConnect:TextView? = null
    var mqttmessage: StringTokenizer? =null
    var temp:String? =null
    var hum:String? = null

    var charttmp: LineChart? = null
    var msChart:ChartHelper? = null



    //FOR DATAS
    // 2 - Identify each fragment with a number
    private val FRAGMENT_DASH = 0
    private val FRAGMENT_TEMP = 1
    private val FRAGMENT_HUM = 2
    private val FRAGMENT_TOOL = 3
    private val FRAGMENT_CONNECT = 4
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)



        startactivity()


        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

     override fun onButtonClicked(view: View) {
        ledOn()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        when (item.itemId) {


            R.id.action_settings ->{
                val j: Intent = Intent(this,Settings::class.java)
                j.putExtra("User",msUser.user)
                j.putExtra("Password",msUser.password)
                j.putExtra("Port",msUser.port)
                j.putExtra("Server",msUser.url)
               startActivity(j)
                finish()
                return true
            }
            R.id.action_disconnect ->{
                val j: Intent = Intent(this,LoginActivity::class.java)
                startActivity(j)
                finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)

        }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                showFragment(FRAGMENT_DASH)


            }
            R.id.nav_gallery -> {
                showFragment(FRAGMENT_TEMP)

            }
            R.id.nav_slideshow -> {
            showFragment(FRAGMENT_HUM)

            }

            R.id.nav_share -> {
                showFragment(FRAGMENT_TOOL)
            }
            R.id.nav_send -> {
                showFragment(FRAGMENT_CONNECT)

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun startactivity() {
        try {
            mqttHelper = ClientMqtt(
                nav_view.context,
                msUser.Serveruri,
                msUser.clientId,
                msUser.topic,
                msUser.user,
                msUser.password
            )
            mqttHelper.setCallback(object : MqttCallbackExtended {
                override fun connectComplete(b: Boolean, s: String) {
                    //msConnect.setText("Connected")
                    //msConnect.setTextColor(-16711936)


                }

                override fun connectionLost(throwable: Throwable) {



                }

                @Throws(Exception::class)
                override fun messageArrived(topic: String, mqttMessage: MqttMessage) {
                    Log.w("Debug", mqttMessage.toString())
                    mqttmessage = StringTokenizer(mqttMessage.toString(),":")
                    temp = mqttmessage!!.nextToken()
                    hum = mqttmessage!!.nextToken()

                    var huma = ArrayList<String>()
                    huma.add(hum.toString())
                    Log.w("Debug",huma.isEmpty().toString())


                    var hum1:String = BigDecimal(((hum.toString().toDouble() / 850)*100)).setScale(2, RoundingMode.HALF_EVEN).toString()
                    hum1 = hum1 + " %"

                    if(fragmentDashBoard!!.isVisible){
                        var tet:TextView = fragmentDashBoard!!.view!!.findViewById(R.id.textView4)
                        var tmp:TextView = fragmentDashBoard!!.view!!.findViewById(R.id.text_tmp2)
                        tet.setText(hum1)
                        tmp.setText(temp +" C")
                    }
                    Log.w("temppp",fragmentTemp!!.isVisible.toString())

                    if(fragmentTemp!!.isVisible){
                        charttmp = fragmentTemp!!.view!!.findViewById(R.id.chart2)
                        msChart = ChartHelper(charttmp)
                        msChart!!.addEntry(temp.toString().toFloat())

                    }
                    Log.w("hummm",fragmentHumidity!!.isVisible.toString())

                    if(fragmentHumidity!!.isVisible){
                        var chartHum:LineChart = fragmentHumidity!!.view!!.findViewById(R.id.chart1)
                       var  msCharth = ChartHelper(chartHum)

                    //if(huma.isNotEmpty()) {

                        for (x in 0 until huma.size step 1) {
                            msCharth.addEntry(huma.get(x).toFloat())
                            Log.w("caca",huma.get(x))
                            Log.w("caca",huma.size.toString())
                        }

                    //}
                    }
                  // arcGauge.value = mqttMessage.toString().toDouble()

                }

                override fun deliveryComplete(iMqttDeliveryToken: IMqttDeliveryToken) {

                }
            })

            if (mqttHelper.connected){
                //msConnect.setText("Connected")
                //msConnect.setTextColor(-16711936)
            }
            else{
                //msConnect.setText("No Connected")
                //msConnect.setTextColor(Color.RED)
            }
        } catch (e:Exception) {

        }


    }



        private fun showFragment(fragmentIdentifier:Int) {
            when (fragmentIdentifier) {

                FRAGMENT_DASH -> {
                    this.showDashFragment()

                }

                FRAGMENT_TEMP -> {
                    this.showTempFragment()
                }
                FRAGMENT_HUM -> {
                    this.showHumFragment()
                }
                FRAGMENT_TOOL ->{
                    this.showToolFragment()
                }
                FRAGMENT_CONNECT->{
                    this.showCoonectFragment()

                }
            }
        }

        // ---

        // 4 - Create each fragment page and show it

        private fun showDashFragment(){
            if (this.fragmentDashBoard == null) this.fragmentDashBoard = FragementDashBoard.newInstance()
            this.startTransactionFragment(this.fragmentDashBoard!!)
        }

        private fun showTempFragment(){
            if (this.fragmentTemp== null) this.fragmentTemp = FragementTemp.newInstance()
            this.startTransactionFragment(this.fragmentTemp!!)
        }

        private fun showHumFragment(){
            if (this.fragmentHumidity == null) this.fragmentHumidity = FragementHum.newInstance()
            this.startTransactionFragment(this.fragmentHumidity!!)

        }
    private fun showToolFragment(){
        if (this.fragmentTool == null) this.fragmentTool = FragementTool.newInstance()
        this.startTransactionFragment(this.fragmentTool!!)

    }
    private fun showCoonectFragment(){
        if (this.fragmentConnect == null) this.fragmentConnect = FragementConnect.newInstance()
        this.startTransactionFragment(this.fragmentConnect!!)
    }

        // ---

        // 3 - Generic method that will replace and show a fragment inside the MainActivity Frame Layout
        private fun startTransactionFragment(fragment:Fragment){
            if (!fragment.isVisible()){
                getSupportFragmentManager().beginTransaction()
                    .replace(R.id.connected, fragment).commit()
            }
        }

    private fun ledOn() {
        try {
            mqttSender = ClientMqttSend(
                nav_view.context,
                msUser.Serveruri,
                msUser.clientId,
                "motor/",
                msUser.user,
                msUser.password
            )

            mqttHelper.setCallback(object : MqttCallbackExtended {
                override fun connectComplete(b: Boolean, s: String) {
                    //msConnect.setText("Connected")




                }

                override fun connectionLost(throwable: Throwable) {



                }

                @Throws(Exception::class)
                override fun messageArrived(topic: String, mqttMessage: MqttMessage) {
                    Log.w("Debug", mqttMessage.toString())

                    // arcGauge.value = mqttMessage.toString().toDouble()

                }

                override fun deliveryComplete(iMqttDeliveryToken: IMqttDeliveryToken) {

                }
            })

        }catch (e:java.lang.Exception)  {

        }
    }

    }
