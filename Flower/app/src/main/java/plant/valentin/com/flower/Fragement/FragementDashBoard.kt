package plant.valentin.com.flower.Fragement

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import plant.valentin.com.flower.R
import plant.valentin.com.flower.Fragement.FragementDashBoard.OnButtonClickedListener



class FragementDashBoard: Fragment() ,View.OnClickListener {

   lateinit var text_hum:TextView

    private var mCallback: OnButtonClickedListener? = null

    // 1 - Declare our interface that will be implemented by any container activity
    interface OnButtonClickedListener {
        fun onButtonClicked(view: View)
    }
    companion object {

        fun newInstance(): FragementDashBoard {
            return FragementDashBoard()
        }
    }

    //3
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View = inflater.inflate(R.layout.activity_dash, container, false)
        view.findViewById<Button>(R.id.button).setOnClickListener(this)
        return  view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        // 4 - Call the method that creating callback after being attached to parent activity
        this.createCallbackToParentActivity()
    }

    override fun onClick(v: View) {
        // 5 - Spread the click to the parent activity
        mCallback!!.onButtonClicked(v)
    }

    // --------------
    // FRAGMENT SUPPORT
    // --------------

    // 3 - Create callback to parent activity
    private fun createCallbackToParentActivity() {
        try {
            //Parent activity will automatically subscribe to callback
            mCallback = activity as OnButtonClickedListener?
        } catch (e: ClassCastException) {
            throw ClassCastException(e.toString() + " must implement OnButtonClickedListener")
        }

    }
}
