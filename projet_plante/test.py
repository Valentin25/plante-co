#! /usr/bin/env python3
# _*_ coding: utf8 _*_
import json
import paho.mqtt.client as mqtt
import random
import time
import threading
import Adafruit_DHT
import serial
import RPi.GPIO as GPIO 

def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))
    
def on_message(client, userdata, message):
    print("coucou le message")
    set_motor()
    if message.retain==1:
        print("This is a retained message")

mqttc = mqtt.Client("client1", clean_session = False)
mqttc.username_pw_set("zzwusncv", "qggmskLbgw3G")
mqttc.on_subscribe = on_subscribe
mqttc.on_message = on_message
mqttc.connect("farmer.cloudmqtt.com", 13447, 60, "")

def set_motor():
	pompe = 17
	try:

		# Configure les pins
		GPIO.setmode(GPIO.BCM)

		GPIO.setup(pompe, GPIO.OUT)
		GPIO.output(pompe, GPIO.HIGH)
		time.sleep(5)
		GPIO.output(pompe, GPIO.LOW)
	except KeyboardInterrupt:
		pass

def lecture_capteurs():
    print("lecture temp")
    pin = 4
    sensor = 11
    humidity, temperature = Adafruit_DHT.read_retry(sensor,pin)
    if humidity is not None and temperature is not None:
        print('Temp={0:0.1f}*  Humidity={1:0.1f}%'.format(temperature,humidity))
       
    else:
        print('Failed to get reading. Try again!')
    print("lecture moisture sensor")
    serialArduino = serial.Serial('/dev/ttyACM0',9600)
    moisture = str(serialArduino.readline())
    moisture = moisture.replace("b'","")
    moisture = moisture.replace("\\r\\n'","")
    print(moisture)
    data = str(temperature) + ":" + moisture
    pub(data)


def moteur():
	time.sleep(3)
	result, mid =mqttc.subscribe("motor/")
	print(result, mid)
	time.sleep(3)
	mqttc.on_message = on_message
	mqttc.loop_forever()
	
	
	
	
def pub(sensor_value):
	mqttc.publish("sensor/temp", payload = sensor_value, qos = 0)
	threading.Timer(1, lecture_capteurs).start()
	print("données envoyées")




t1 = threading.Thread(target= lecture_capteurs)
t1.start()


t2 = threading.Thread(target= moteur)
t2.start()

t1.join()
t2.join()
print("done")
